from django.shortcuts import render, redirect
from .models import Status, Color
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
import requests
from lxml import html

# Create your views here.


def index(request, message_type=''):
    status = Status.objects.order_by('-id')
    response_corona = requests.get(
        'https://widget.kompas.com/corona/statistik')
    page_tree = html.fromstring(response_corona.content)
    positive_case = page_tree.xpath(
        '//div[@class="covid__box2 -cases"]/text()[1]')[0]

    message = ''
    if message_type == 'success':
        message = "Success post new status"
    if message_type == 'status_empty':
        message = "Please fill status field, status cannot be blank"
    return render(request, 'index.html', {'statuses': status, 'message': message, 'message_type': message_type, 'corona_case': positive_case})


def add_status(request):
    if request.method == 'POST':
        if request.POST['status'] != '':
            warning = ''
            if request.POST['nama'] != '':
                nama = request.POST['nama']
            else:
                nama = 'Anonymous'
                warning = 'Your status will showed from anonymous'
            data = {
                'name': nama,
                'status': request.POST['status'],
                'warning': warning
            }
            return render(request, 'status-confirm.html', data)
        else:
            return index(request, message_type='status_empty')
    return redirect('/')


def confirm_status(request):
    if request.method == 'POST':
        if request.POST['confirm'] == 'yes':
            data = request.POST
            Status.objects.create(nama=data['nama'], pesan=data['status'])
            return index(request, message_type='success')
    return redirect('/')


@csrf_exempt
def add_like(request):
    if request.method == 'POST':
        try:
            post = Status.objects.get(id=request.POST['postId'])
            post.like_count += 1
            post.save()
            return JsonResponse({
                'likeCount': post.like_count
            })
        except Status.DoesNotExist:
            return HttpResponseNotFound()
    return HttpResponseBadRequest()


@csrf_exempt
def remove_like(request):
    if request.method == 'POST':
        try:
            post = Status.objects.get(id=request.POST['postId'])
            if post.like_count > 0:
                post.like_count -= 1
            post.save()
            return JsonResponse({
                'likeCount': post.like_count
            })
        except Status.DoesNotExist:
            return HttpResponseNotFound()
    return HttpResponseBadRequest()

def change_color(request, postid):
    try:
        status = Status.objects.get(id=postid)
        try: 
            current_color_id = status.color.id
        except:
            current_color_id = 0
        
        max_color_id = Color.objects.latest('id').id
        if current_color_id+1 > max_color_id:
            color = Color.objects.all().first()
        else:
            color = Color.objects.filter(id__gt=current_color_id).order_by('id').first()

        status.color = color
        status.save()
    except Status.DoesNotExist:
        pass
    return redirect('/')