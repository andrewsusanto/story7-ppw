from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

import os

from .models import Status, Color
from .views import index
from faker import Faker

# Create your tests here.


class StatusTestCase(TestCase):
    def test_models_create(self):
        Status.objects.create(
            nama='Andi',
            pesan='Test'
        )

    def test_check_landing_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_check_landing_page_have_form(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)

    def test_check_show_status_landing_page(self):
        faker = Faker()
        for i in range(5):
            status = Status.objects.create(
                nama=faker.name(), pesan=faker.text())

            response = self.client.get('/')

            # Use assertContains (from response) , use assertIn (must precedded with content.decode)
            self.assertEqual(response.status_code, 200)
            self.assertContains(response, status.nama)
            self.assertContains(response, status.pesan)

    def test_check_form_submit_url(self):
        response = self.client.get('/addstatus/')
        self.assertEqual(response.status_code, 302)

    def test_check_form_submit_contains_post_data(self):
        faker = Faker()
        name = faker.name()
        status = faker.text()
        response = self.client.post('/addstatus/', data={
            'nama': name,
            'status': status
        })
        self.assertContains(response, name)
        self.assertContains(response, status)

    def test_check_form_submit_confirm_success_model(self):
        faker = Faker()
        name = faker.name()
        status = faker.text()
        response = self.client.post('/confirmstatus/', data={
            'nama': name,
            'status': status,
            'confirm': 'yes'
        })
        self.assertEqual(response.status_code, 200)

        # Checking data in models
        data = Status.objects.get(nama=name)
        self.assertEqual(data.nama, name)
        self.assertEqual(data.pesan, status)

        # Checking new data in landing page
        response = self.client.get('/')
        self.assertContains(response, name)
        self.assertContains(response, status)

    def test_check_no_name_and_no_status(self):
        response = self.client.post('/addstatus/', data={
            'nama': '',
            'status': ''
        })

        # Status cannot be empty
        self.assertEqual(response.status_code, 200)

    def test_check_ajax_backend_for_add_like_model_not_exist(self):
        response = self.client.post('/postlike/add/', {
            'postId': 1
        })
        self.assertEqual(response.status_code, 404)

    def test_check_ajax_backend_for_add_like_bad_request(self):
        response = self.client.get('/postlike/add/')
        self.assertEqual(response.status_code, 400)

    def test_check_ajax_backend_for_add_like_model_exist(self):
        faker = Faker()

        data = Status.objects.create(nama=faker.name(), pesan=faker.text())
        response = self.client.post('/postlike/add/', {
            'postId': data.id
        })
        self.assertEqual(response.status_code, 200)

        data = Status.objects.get(id=data.id)
        self.assertEqual(data.like_count, 1)

    def test_check_ajax_backend_for_remove_like_model_not_exist(self):
        response = self.client.post('/postlike/remove/', {
            'postId': 1
        })
        self.assertEqual(response.status_code, 404)

    def test_check_ajax_backend_for_remove_like_bad_request(self):
        response = self.client.get('/postlike/remove/')
        self.assertEqual(response.status_code, 400)

    def test_check_ajax_backend_for_remove_like_model_exist(self):
        faker = Faker()

        data = Status.objects.create(
            nama=faker.name(), pesan=faker.text(), like_count=1)
        response = self.client.post('/postlike/remove/', {
            'postId': data.id
        })
        self.assertEqual(response.status_code, 200)

        data = Status.objects.get(id=data.id)
        self.assertEqual(data.like_count, 0)

    def test_check_corona_info_on_status_section(self):
        response = self.client.get('/')
        self.assertContains(response, 'corona')

    def test_check_model_return_str(self):
        faker = Faker()

        name = faker.name()
        status = faker.text()
        data = Status.objects.create(nama=name, pesan=status)

        self.assertEqual(str(data), name + ' - ' + status)

    def test_check_confirm_anonymous(self):
        faker = Faker()

        status = faker.text()
        response = self.client.post('/addstatus/', {
            'nama': '',
            'status': status
        })

        self.assertContains(response, 'anonymous')

    def test_check_redirect_confirm_status_refreshed(self):
        response = self.client.get('/confirmstatus/')

        self.assertEqual(response.status_code, 302)

    def test_check_not_error_when_changing_color_non_exist_post(self):
        response = self.client.get('/changecolor/1/')

        self.assertEqual(response.status_code, 302)

    def test_check_changing_color_back_to_begining(self):
        faker = Faker()
        name = faker.name()
        status = faker.text()
        response = self.client.post('/confirmstatus/', data={
            'nama': name,
            'status': status,
            'confirm': 'yes'
        })
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, name)

        test_color = ['#D1C4E9', '#B2EBF2']
        for color in test_color:
            Color.objects.create(color_code = color)

        data = Status.objects.get(nama=name)
        for x in range(len(test_color)+1):
            self.client.get('/changecolor/'+str(data.id)+'/')

        data = Status.objects.get(nama=name)
        
        self.assertEqual(data.color.color_code, test_color[0])



class StatusFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

        super().tearDown()

    def test_submit_form_from_landing_page_confirm_with_data(self):
        faker = Faker()

        name = faker.name()
        status = faker.text()

        self.browser.get(self.live_server_url + '/')
        name_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'name')]/input")
        status_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'status')]/textarea")
        submit = self.browser.find_element_by_xpath("//button[@type='submit']")

        name_input.send_keys(name)
        status_input.send_keys(status)

        submit.click()

        # Checking confirm page showing name and status submitted
        self.assertIn(name, self.browser.page_source)
        self.assertIn(status, self.browser.page_source)

    def test_submit_form_from_landing_page_confirm_post(self):
        faker = Faker()

        name = faker.name()
        status = faker.text()

        self.browser.get(self.live_server_url + '/')

        name_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'name')]/input")
        status_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'status')]/textarea")
        submit = self.browser.find_element_by_xpath("//button[@type='submit']")

        name_input.send_keys(name)
        status_input.send_keys(status)
        submit.click()

        confirm_button = self.browser.find_element_by_xpath(
            "//button[@type='submit']")

        confirm_button.click()

        # Checking result on landing page and on model
        self.browser.get(self.live_server_url + '/')
        self.assertIn(name, self.browser.page_source)
        self.assertIn(status, self.browser.page_source)

    def test_submit_form_from_landing_page_reject_post(self):
        faker = Faker()

        name = faker.name()
        status = faker.text()

        self.browser.get(self.live_server_url + '/')

        name_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'name')]/input")
        status_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'status')]/textarea")
        submit = self.browser.find_element_by_xpath("//button[@type='submit']")

        name_input.send_keys(name)
        status_input.send_keys(status)
        submit.click()

        discard_button = self.browser.find_element_by_xpath(
            "//a[contains(.,'Discard')]")

        discard_button.click()

        # Checking result on landing page and on model
        self.browser.get(self.live_server_url + '/')
        self.assertNotIn(name, self.browser.page_source)
        self.assertNotIn(status, self.browser.page_source)

    def test_show_warning_when_name_is_empty(self):
        faker = Faker()

        name = ''
        status = faker.text()

        self.browser.get(self.live_server_url + '/')

        name_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'name')]/input")
        status_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'status')]/textarea")
        submit = self.browser.find_element_by_xpath("//button[@type='submit']")

        name_input.send_keys(name)
        status_input.send_keys(status)
        submit.click()

        self.assertIn('anonymous', self.browser.page_source.lower())

        confirm_button = self.browser.find_element_by_xpath(
            "//button[@type='submit']")

        confirm_button.click()

        self.browser.get(self.live_server_url + '/')
        self.assertIn('anonymous', self.browser.page_source.lower())

    def test_show_success_message(self):
        faker = Faker()

        name = faker.name()
        status = faker.text()

        self.browser.get(self.live_server_url + '/')

        name_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'name')]/input")
        status_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'status')]/textarea")
        submit = self.browser.find_element_by_xpath("//button[@type='submit']")

        name_input.send_keys(name)
        status_input.send_keys(status)
        submit.click()

        confirm_button = self.browser.find_element_by_xpath(
            "//button[@type='submit']")

        confirm_button.click()

        self.assertIn('success', self.browser.page_source.lower())

    def test_show_warning_when_status_is_empty(self):
        faker = Faker()

        name = faker.name()
        status = ''

        self.browser.get(self.live_server_url + '/')

        name_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'name')]/input")
        status_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'status')]/textarea")
        submit = self.browser.find_element_by_xpath("//button[@type='submit']")

        name_input.send_keys(name)
        status_input.send_keys(status)
        submit.click()

        self.assertIn('empty', self.browser.page_source.lower())

    def test_change_color_in_published_status(self):
        # Insert test color to db
        test_color = ['#D1C4E9', '#C5CAE9', '#BBDEFB', '#B3E5FC', '#B2EBF2']
        for color in test_color:
            Color.objects.create(color_code = color)

        faker = Faker()

        name = faker.name()
        status = faker.text()

        self.browser.get(self.live_server_url + '/')

        name_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'name')]/input")
        status_input = self.browser.find_element_by_xpath(
            "//div[contains(.,'status')]/textarea")
        submit = self.browser.find_element_by_xpath("//button[@type='submit']")

        name_input.send_keys(name)
        status_input.send_keys(status)
        submit.click()

        confirm_button = self.browser.find_element_by_xpath(
            "//button[@type='submit']")

        confirm_button.click()

        status_card = self.browser.find_element_by_xpath("//div[@class='status-card'][contains(.,'"+status+"')][1]")
        color_before = status_card.value_of_css_property('background-color')

        change_color_button = self.browser.find_element_by_xpath("//div[@class='status-card'][contains(.,'"+status+"')]/div[3]/a")
        change_color_button.click()

        status_card_after = self.browser.find_element_by_xpath("//div[@class='status-card'][contains(.,'"+status+"')][1]")
        color_after = status_card_after.value_of_css_property('background-color')

        self.assertTrue(color_before != color_after)

    def test_change_color_four_different_color_in_published_status(self):
        # Insert test color to db
        test_color = ['#D1C4E9', '#C5CAE9', '#BBDEFB', '#B3E5FC', '#B2EBF2']
        for color in test_color:
            Color.objects.create(color_code = color)
        
        background_color = []

        # Submitting status four times
        for x in range(4):
            faker = Faker()

            name = faker.name()
            status = faker.text()

            self.browser.get(self.live_server_url + '/')

            name_input = self.browser.find_element_by_xpath(
                "//div[contains(.,'name')]/input")
            status_input = self.browser.find_element_by_xpath(
                "//div[contains(.,'status')]/textarea")
            submit = self.browser.find_element_by_xpath("//button[@type='submit']")

            name_input.send_keys(name)
            status_input.send_keys(status)
            submit.click()

            confirm_button = self.browser.find_element_by_xpath(
                "//button[@type='submit']")

            confirm_button.click()

            # Click change color different times for every status
            for i in range(x+1):
                change_color_button = self.browser.find_element_by_xpath("//div[@class='status-card'][contains(.,'"+status+"')]/div[3]/a")
                change_color_button.click()
            
            # Get background-color for every status (Newest status should be on top)
            status_card = self.browser.find_element_by_xpath("//div[@class='status-card'][contains(.,'"+status+"')][1]")
            color = status_card.value_of_css_property('background-color')
            background_color.append(color)

        # Check every duplicate background_color
        for x in background_color:
            self.assertEqual(background_color.count(x), 1)
