from django.contrib import admin
from .models import Status, Color

# Register your models here.
admin.site.register(Status)
admin.site.register(Color)