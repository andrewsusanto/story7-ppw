from django.db import models

class Color(models.Model):
    color_code = models.CharField(max_length = 16, unique=True)

# Create your models here.
class Status(models.Model):
    nama = models.CharField(max_length=32)
    pesan = models.TextField()
    like_count = models.IntegerField(default=0)
    created_date = models.DateTimeField(auto_now=True)
    color = models.ForeignKey(Color, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.nama + ' - ' + self.pesan
